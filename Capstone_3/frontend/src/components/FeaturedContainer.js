import Carousel from 'react-bootstrap/Carousel';
import Container from 'react-bootstrap/Container';
import './css/Carousel.css';

function FeaturedContainer() {
  return (
    <>
      <h1 className="text-center">Featured Products</h1>
      <Container fluid className="d-flex justify-content-center">
        <img
          className="position-absolute w-50"
          src={require('../assets/img/woodtray.png')}
          alt="Basket"
        />
        <Carousel variant="dark" controls={false} indicators={false}>
          <Carousel.Item className="mb-5 mb-xl-0">
            <img
              className="d-block mx-auto w-50"
              src={require('../assets/img/melon.png')}
              alt="First slide"
            />
            <Carousel.Caption className="mt-xl-5 carousel-caption">
              <h1>Melon Bread</h1>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className="mb-5 mb-xl-0">
            <img
              className="d-block mx-auto w-50"
              src={require('../assets/img/pandesal.png')}
              alt="Second slide"
            />
            <Carousel.Caption className="mt-xl-5 carousel-caption">
              <h1>Pandesal</h1>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item className="mb-5 mb-xl-0">
            <img
              className="d-block mx-auto w-50"
              src={require('../assets/img/waffle.png')}
              alt="Third slide"
            />
            <Carousel.Caption className="mt-xl-5 carousel-caption">
              <h1>Waffle</h1>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>
    </>
  );
}

export default FeaturedContainer;
