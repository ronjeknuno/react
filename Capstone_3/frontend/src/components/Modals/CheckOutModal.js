import React, { useState, useEffect, useContext } from 'react';
import {
  Form,
  Card,
  Button,
  Container,
  Row,
  Col,
  Modal,
} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../context/UsersContext';
import ProductContext from '../../context/ProductsContext';

import { useProductsContext } from '../../hooks/useProductsContext';

function CheckedOutModal({ productID }) {
  const [show, setShow] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [total, setTotal] = useState(productID.price);

  useEffect(() => {
    setTotal(quantity * productID.price);
  }, [quantity]);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const checkOut = (event) => {
    event.preventDefault();
    Swal.fire({
      title: 'Successful',
      icon: 'success',
      text: 'Product Successfully Added',
    });
    setShow(false);
  };

  return (
    <>
      <Button onClick={handleShow} variant="secondary">
        CheckOut
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title className="text-center">Check Out Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            // onSubmit={editProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Card>
              <Card.Header as="h5">{productID.name}</Card.Header>
              <Card.Body>
                <Card.Title>{productID.description}</Card.Title>
                <Card.Text>
                  <Card.Text>
                    Price: <strong>&#8369; {productID.price}.00</strong>
                  </Card.Text>
                </Card.Text>
                Quantity:
                <input
                  className="col-1"
                  type={'number'}
                  value={quantity}
                  onChange={(event) => setQuantity(event.target.value)}
                  required
                ></input>
                <Card.Text className="mt-3">
                  Total: <strong>&#8369; {total}.00</strong>
                </Card.Text>
              </Card.Body>
            </Card>

            <Modal.Footer className="">
              <Button variant="secondary" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="primary" type="submit" onClick={checkOut}>
                CheckOut
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default CheckedOutModal;
