import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../../context/UsersContext';
import ProductContext from '../../context/ProductsContext';

import { useProductsContext } from '../../hooks/useProductsContext';

function EditProductModal({ productID }) {
  //   console.log(productID);
  const { dispatch } = useProductsContext();
  const [show, setShow] = useState(false);
  const [name, setName] = useState(productID.name);
  const [description, setDescription] = useState(productID.description);
  const [price, setPrice] = useState(productID.price);
  const [stocks, setStocks] = useState(productID.stocks);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const editProductHandler = (event) => {
    event.preventDefault();

    const updateProducts = async () => {
      const response = await fetch(`/products/updateProduct/${productID._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
        body: JSON.stringify({
          name,
          description,
          price,
          stocks,
        }),
      });
      const json = await response.json();
      if (response.ok) {
        Swal.fire({
          title: 'Successfully Updated!',
          icon: 'success',
          text: json.message,
        });
        setShow(false);
      }
    };
    updateProducts();
  };
  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(`/products/allProducts`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
      });
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, [show]);

  return (
    <>
      <Button onClick={handleShow} variant="info">
        Edit
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            onSubmit={editProductHandler}
            className="col-12 px-3 was-invalidated "
          >
            <Form.Group className="">
              <Form.Label className="col-12">
                Bread Name
                <Form.Control
                  type="text"
                  placeholder="Name the Bread"
                  value={name}
                  onChange={(event) => setName(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>
            <Form.Group className="mb-2">
              <Form.Label>Bread Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Describe the kind of Bread"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-5 d-flex flex-row align-items-center justify-content-between">
              <Form.Label className="col-5">
                Price
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={price}
                  onChange={(event) => setPrice(event.target.value)}
                  required
                />
              </Form.Label>

              <Form.Label className="col-5">
                Stocks
                <Form.Control
                  type="number"
                  placeholder="Enter Last Name"
                  value={stocks}
                  onChange={(event) => setStocks(event.target.value)}
                  required
                />
              </Form.Label>
            </Form.Group>

            <Modal.Footer className="">
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" type="submit">
                Save
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default EditProductModal;
