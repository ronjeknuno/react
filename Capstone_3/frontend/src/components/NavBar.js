import {
  Container,
  Nav,
  Navbar,
  NavDropdown,
  DropdownButton,
  Dropdown,
  Offcanvas,
  Form,
  Button,
} from 'react-bootstrap';

import { Link } from 'react-router-dom';
import './css/NavBar.css';
import LoginModal from './Modals/LoginModal';
import RegisterModal from './Modals/RegisterModal';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../context/UsersContext';
import { useContext, useEffect } from 'react';

import { useUsersContext } from '../hooks/useUsersContext';

function NavBar() {
  const { users, dispatch } = useUsersContext();
  console.log(users);

  // useEffect(() => {
  //   fetch(`/users/profile`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
  //     },
  //   })
  //     .then((response) => response.json())
  //     .then((data) => {
  //       setUser({ id: data._id, isAdmin: data.isAdmin });
  //     });
  // }, [user]);

  const history = useNavigate();
  function logout() {
    localStorage.removeItem('accessToken');
    dispatch({ type: 'SET_USERS', payload: null });
    Swal.fire({
      title: `See You Again Later`,
      html: `GoodLuck to Your Journey`,
      icon: 'info',
    });
    history('/');
  }
  // console.log(user);

  return (
    <>
      <div id="top">
        <span>Alrights Reserved</span>
      </div>
      <Navbar key="xl" bg="light" expand="xl" className="">
        <Navbar.Brand href="#home">
          <img
            className="d-xl-none position-absolute  logo-md"
            src={require('../assets/icon/pantasia-logo.png')}
            alt=""
            width={'135px'}
          />
        </Navbar.Brand>
        <Container>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-xl`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-xl`}
            aria-labelledby={`offcanvasNavbarLabel-expand-xl`}
            placement="start"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id="pantasia"></Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-between flex-grow-1">
                <div className="d-flex align-items-center flex-column flex-xl-row">
                  <Nav.Link as={Link} to="/home">
                    Home
                  </Nav.Link>
                  <Nav.Link className="mx-5" as={Link} to="/shop">
                    Shop
                  </Nav.Link>
                  <Nav.Link href="#link">About Us</Nav.Link>
                </div>
                <div></div>
                <Navbar.Brand
                  href="#home"
                  className="d-none d-xl-block text-center position-absolute logo-lg"
                >
                  {/* LArger Device */}
                  <img
                    className="logo-lg"
                    src={require('../assets/icon/pantasia-logo.png')}
                    alt=""
                  />
                </Navbar.Brand>
                <div className="d-flex align-items-center flex-xl-row flex-column">
                  <Nav.Link href="#link">FAQs</Nav.Link>
                  <Nav.Link className="mx-5" href="#link">
                    Contact
                  </Nav.Link>
                  <div className="d-none d-lg-block">
                    {users && users ? (
                      <>
                        <NavDropdown
                          title="Acoount"
                          id="dropdown-button-drop-down-centered"
                        >
                          <NavDropdown.Item>Cart</NavDropdown.Item>
                          <NavDropdown.Item>{users.userName}</NavDropdown.Item>
                          <NavDropdown.Item>{users.email}</NavDropdown.Item>
                          <NavDropdown.Divider />
                          <NavDropdown.Item onClick={logout}>
                            Logout
                          </NavDropdown.Item>
                        </NavDropdown>
                      </>
                    ) : (
                      <>
                        <div className="d-none d-xl-block">
                          <NavDropdown
                            title="Account"
                            id="dropdown-button-drop-down-centered"
                          >
                            <NavDropdown.Item>
                              <LoginModal />
                            </NavDropdown.Item>
                            <NavDropdown.Item>
                              <RegisterModal />
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                            {/* <NavDropdown.Item href="/#" onClick={logout}>
                        Logout
                      </NavDropdown.Item> */}
                          </NavDropdown>
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
          <div className="d-xl-none">
            {users && users ? (
              <>
                <NavDropdown
                  title="Acoount"
                  id="dropdown-button-drop-down-centered"
                >
                  {!users.isAdmin && <NavDropdown.Item>Cart</NavDropdown.Item>}

                  <NavDropdown.Item>{users.userName}</NavDropdown.Item>
                  <NavDropdown.Item>{users.email}</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
                </NavDropdown>
              </>
            ) : (
              <>
                <NavDropdown
                  title="Acoount"
                  id="dropdown-button-drop-down-centered"
                >
                  <NavDropdown.Item>
                    <LoginModal />
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <RegisterModal />
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  {/* <NavDropdown.Item href="/#" onClick={logout}>
                        Logout
                      </NavDropdown.Item> */}
                </NavDropdown>
              </>
            )}
          </div>
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;
