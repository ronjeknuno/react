import React from 'react';
import { Container, Col } from 'react-bootstrap';
import '../components/css/Page404.css';
import { Link } from 'react-router-dom';

function Page404() {
  return (
    <Container className="d-flex flex-column align-items-center justify-content-center main-container">
      <h1>Page Not Found</h1>
      <h4>Error404</h4>
      <Link to={'/'}>Go Back</Link>
    </Container>
  );
}

export default Page404;
