import { useEffect } from 'react';
import { useProductsContext } from '../hooks/useProductsContext';
import {
  Container,
  Row,
  Col,
  Nav,
  Navbar,
  NavDropdown,
  Offcanvas,
  Form,
  Button,
} from 'react-bootstrap';
import NavBar from '../components/NavBar';
import ProductTable from '../components/Admin/ProductTable';

function AdminPage() {
  const { products, dispatch } = useProductsContext();

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(`/products/allProducts`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
      });
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, []);
  // console.log(products);

  return (
    <>
      <Container fluid>
        <NavBar />
        {products && <ProductTable products={products} />}
      </Container>
    </>
  );
}

export default AdminPage;
