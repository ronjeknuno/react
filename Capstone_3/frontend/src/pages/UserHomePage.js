import { useNavigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../context/UsersContext';
import { Container, Button } from 'react-bootstrap';
import NavBar from '../components/NavBar';
import Landing from '../components/Landing';
import FeaturedContainer from '../components/FeaturedContainer';
import Footer from '../components/Footer';
import { useTraysContext } from '../hooks/useTraysContext';
function UserHomePage() {
  return (
    <div>
      <NavBar />
      <Landing />
      <FeaturedContainer />
      <Footer />
    </div>
  );
}

export default UserHomePage;
