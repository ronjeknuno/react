import React from 'react';
import FeaturedContainer from '../components/FeaturedContainer';
import Footer from '../components/Footer';
import Landing from '../components/Landing';
import NavBar from '../components/NavBar';
import ProductContainer from '../components/ProductContainer';
import { useEffect } from 'react';
import { useProductsContext } from '../hooks/useProductsContext';
import Jumbotron from '../components/Jumbotron';
import { useTraysContext } from '../hooks/useTraysContext';
import TrayContainer from '../components/TrayContainer';
function UserProductPage() {
  const { products, dispatch } = useProductsContext();

  useEffect(() => {
    const fetchProducts = async () => {
      const response = await fetch(`/products`);
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_PRODUCTS', payload: json });
      }
    };
    fetchProducts();
  }, []);

  const { trays, trayDispatch } = useTraysContext();
  // useEffect(() => {
  //   const fetchTrays = async () => {
  //     const response = await fetch(`/trays/639801c2de1528b53d536043`, {
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
  //       },
  //     });
  //     const json = await response.json();
  //     console.log(json);
  //     if (response.ok) {
  //       trayDispatch({ type: 'SET_TRAYS', payload: json.result });
  //     }
  //   };
  //   fetchTrays();
  // }, []);

  // console.log(trays);
  console.log(trays);
  console.log(products);

  return (
    <div>
      <NavBar />
      <Jumbotron />
      {products && <ProductContainer products={products} />}

      {/* {trays && <TrayContainer trays={trays} />} */}

      <Footer />
    </div>
  );
}

export default UserProductPage;
