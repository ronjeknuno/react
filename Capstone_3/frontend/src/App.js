import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom';

// import { UserProvider } from './context/UsersContext';
import { useUsersContext } from './hooks/useUsersContext';

import { UsersContextProvider } from './context/UsersContext';

import { useState, useEffect } from 'react';

import Home from './pages/Home';
import 'bootstrap/dist/css/bootstrap.min.css';

import AdminPage from './pages/AdminPage';
import UserHomePage from './pages/UserHomePage';
import UserProductPage from './pages/UserProductPage';
import Page404 from './pages/Page404';
import UserTrayPage from './pages/UserTrayPage';

function App() {
  const { users, dispatch } = useUsersContext();

  // const [user, setUser] = useState({ id: null, isAdmin: false });
  // const unSetUser = () => {
  //   localStorage.removeItem('accessToken');
  // };

  // const [products, setProducts] = useState();
  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
  //     },
  //   })
  //     .then((response) => response.json())
  //     .then((data) => {
  //       setProducts(data.result);
  //     });
  // }, []);
  // console.log(products);

  useEffect(() => {
    const fetchUsers = async () => {
      const response = await fetch(`/users/profile`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
      });
      const json = await response.json();
      if (response.ok) {
        dispatch({ type: 'SET_USERS', payload: json });
      }
    };
    fetchUsers();
  }, []);
  console.log(users);

  // useEffect(() => {
  //   const fetchProducts = async () => {
  //     const response = await fetch(`/trays/639801c2de1528b53d536043`, {
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
  //       },
  //     });
  //     const json = await response.json();
  //     if (response.ok) {
  //       traysDispatch({ type: 'SET_PRODUCTS', payload: json.result });
  //     }
  //   };
  //   fetchProducts();
  // }, []);
  // console.log(trays);

  return (
    <div className="App">
      {/* <ProductProvider value={products}> */}
      <Router>
        <div className="pages">
          <Routes>
            {users && users.isAdmin ? (
              <>
                <Route path="*" element={<Page404 />} />
                <Route path="/" element={<Home />}></Route>
                <Route path="/admin" element={<AdminPage />} />
              </>
            ) : (
              <>
                <Route path="/home" element={<UserHomePage />} />
                <Route path="/shop" element={<UserProductPage />} />
                <Route path="/tray" element={<UserTrayPage />} />
              </>
            )}
            <Route path="/" element={<Home />}></Route>
            <Route path="*" element={<Page404 />} />
          </Routes>
        </div>
      </Router>
      {/* </ProductProvider> */}
    </div>
  );
}

export default App;
