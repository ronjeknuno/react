import { TraysContext } from '../context/TraysContext';
import { useContext } from 'react';

export const useTraysContext = () => {
  const context = useContext(TraysContext);

  if (!context) {
    throw Error('useTrayContext must be used inside a workoutcontextPtovider');
  }
  return context;
};
