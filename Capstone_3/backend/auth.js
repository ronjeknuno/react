const jwt = require('jsonwebtoken');

const secret = 'EcommerceAPI';

// Token creation
module.exports.createAccessToken = (result) => {
  // payload
  const data = {
    id: result._id,
    email: result.email,
    isAdmin: result.isAdmin,
    trayId: result.trayId,
  };
  //   generate json web token using jwt's sign method
  return jwt.sign(data, secret, {
    expiresIn: '30m',
  });
};

// Token verification

module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;

  if (token !== undefined) {
    //   validate the token using verify method, to decrypt the using the secret code.
    token = token.slice(7, token.length);
    console.log(token);

    return jwt.verify(token, secret, (error) => {
      if (error) {
        return response.send(`Invalid Token or Expired Token!`);
      } else {
        next();
      }
    });
  } else {
    return response.send(`Authentication failed! No Token Provided.`);
  }
};

// token decryption

module.exports.decode = (token) => {
  if (token === undefined) {
    return null;
  } else {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return null;
      } else {
        // decode()
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  }
};
